/*
 * Copyright (C) 2024 Dmitry A.Steklenev
 */

#ifndef PM_CLIPBOARD_H
#define PM_CLIPBOARD_H

#include "pm_os2.h"
#include "pm_noncopyable.h"

#ifndef __ccdoc__

/* Standard DDE and clipboard format stings */

#define SZFMT_TEXT        "#1"
#define SZFMT_BITMAP      "#2"
#define SZFMT_DSPTEXT     "#3"
#define SZFMT_DSPBITMAP   "#4"
#define SZFMT_METAFILE    "#5"
#define SZFMT_DSPMETAFILE "#6"
#define SZFMT_PALETTE     "#9"
#define SZFMT_RTF         "Rich Text Format"

#endif

/**
 * Clipboard class.
 *
 * The PMClipboard class provides support for writing data to and reading data
 * from the system clipboard. Although you can only store a single item of
 * data in the clipboard, you can store this item in multiple formats. PMClipboard
 * predefines several system clipboard formats. In addition, any
 * application can create and register additional private formats.
 *
 * You can construct and destruct objects of this class.
 *
 * @author  Dmitry A.Steklenev
 * @version 1.0
 */

class PMClipboard : public PMNonCopyable
{
  public:

    /** Constructs the clipboard object. */
    PMClipboard() {}
    /** Destroys the clipboard object. */
   ~PMClipboard() {}

    /**
     * This function empties the clipboard, removing and freeing
     * all handles to data that is in the clipboard.
     */

    void clear();

    /**
     * Copies the passed data buffer and places it on the
     * clipboard with the format specified.
     *
     *
     * @param szfmt Pointer to one of the system-defined standard format SZFMT_*
     *              or a private clipboard format name.
     * @param data  Pointer to data buffer.
     * @param len   The size of the data to be copied.
     */

    void data( const char* szfmt, const void* data, unsigned long len );

    /** Copies text and places it on the clipboard with the format SZFMT_TEXT. */
    void text( const char* text );

    /**
     * Returns a pointer to the data returned from the clipboard.
     *
     * It is your responsibility to know the type of the value because it
     * is based on the format of the data.
     *
     * @param szfmt Pointer to one of the system-defined standard format SZFMT_*
     *              or a private clipboard format name.
     *
     * @return      A pointer to the data or NULL if data of the specified
     *              format does not exist.
     */

    const void* data( const char* szfmt ) const;

    /** Returns a pointer to the text. */
    const char* text() const;

    /**
     * Returns TRUE if the clipboard has data of the requested format.
     *
     * @param szfmt Pointer to one of the system-defined standard format SZFMT_*
     *              or a private clipboard format name.
     */

    BOOL has_data( const char* szfmt ) const;

    /** Returns TRUE if the clipboard has data with the text format. */
    BOOL has_text() const;

  private:
    /** Returns a clipboard format identifier. */
    unsigned long format( const char* szfmt ) const;
    /** Registers a clipboard format. */
    unsigned long register_format( const char* szfmt );
};

/* Returns TRUE if the clipboard has data of the requested format. */
inline BOOL PMClipboard::has_data( const char* szfmt ) const {
  return data( szfmt ) != 0;
}

/* Returns TRUE if the clipboard has data with the text format. */
inline BOOL PMClipboard::has_text() const {
  return has_data( SZFMT_TEXT );
}

#endif


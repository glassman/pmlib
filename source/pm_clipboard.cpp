/*
 * Copyright (C) 2024 Dmitry A.Steklenev
 */

#include "pm_os2.h"
#include "pm_clipboard.h"
#include "pm_error.h"
#include "pm_gui.h"

/* This function empties the clipboard, removing and freeing
 * all handles to data that is in the clipboard.
 */

void PMClipboard::clear()
{
  if( !WinOpenClipbrd( PMGUI::hab())) {
    PM_THROW_GUIERROR();
  }
  if( !WinEmptyClipbrd( PMGUI::hab())) {
    PM_THROW_GUIERROR();
  }
  if( !WinCloseClipbrd( PMGUI::hab())) {
    PM_THROW_GUIERROR();
  }
}

/* Copies the passed data buffer and places it on the
 * clipboard with the format specified.
 */

void PMClipboard::data( const char* szfmt, const void* p, unsigned long len )
{
  LONG   fmt = register_format( szfmt );
  APIRET rc;
  void*  shared_data;

  rc = DosAllocSharedMem((void**)&shared_data, NULL, len,
                         PAG_WRITE | OBJ_GIVEABLE | PAG_COMMIT );

  if( rc != NO_ERROR ) {
    PM_THROW_DOSERROR(rc);
  }
  if( p ) {
    memcpy( shared_data, p, len );
  }
  if( !WinOpenClipbrd( PMGUI::hab())) {
    PM_THROW_GUIERROR();
  }
  if( !WinSetClipbrdData( PMGUI::hab(), (ULONG)shared_data, fmt, CFI_POINTER )) {
    PM_THROW_GUIERROR();
  }
  if( !WinCloseClipbrd( PMGUI::hab())) {
    PM_THROW_GUIERROR();
  }
}

/* Copies text and places it on the clipboard with the format SZFMT_TEXT.
 */

void PMClipboard::text( const char* text )
{
  if( text ) {
    data( SZFMT_TEXT, text, strlen( text ) + 1 );
  }
}

/* Returns a pointer to the data returned from the clipboard.
 */

const void* PMClipboard::data( const char* szfmt ) const
{
  LONG  fmt = format( szfmt );
  void* shared_data = NULL;

  if( fmt ) {
    if( !WinOpenClipbrd( PMGUI::hab())) {
      PM_THROW_GUIERROR();
    }

    shared_data = (void*)WinQueryClipbrdData( PMGUI::hab(), fmt );

    if( !WinCloseClipbrd( PMGUI::hab())) {
      PM_THROW_GUIERROR();
    }
  }

  return shared_data;
}

/* Returns a pointer to the text. */
const char* PMClipboard::text() const {
  return (const char*)data( SZFMT_TEXT );
}

/* Returns a clipboard format identifier.
 */

ULONG PMClipboard::format( const char* szfmt ) const {
  return WinFindAtom( WinQuerySystemAtomTable(), szfmt );
}

/* Registers a clipboard format.
 */

ULONG PMClipboard::register_format( const char* szfmt )
{
  ATOM id;

  if(( id = WinFindAtom( WinQuerySystemAtomTable(), szfmt )) == 0 ) {
    if(( id = WinAddAtom( WinQuerySystemAtomTable(), szfmt )) == 0 ) {
      PM_THROW_GUIERROR();
    }
  }
  return id;
}

